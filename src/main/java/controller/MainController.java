package controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.Kv;
import com.jfinal.kit.LogKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import interceptor.LoginValidator;
import interceptor.RegisterValidator;
import model.Admin;
import model.Class;
import model.Student;

import java.util.List;


public class MainController extends Controller {


    public void index() {
        renderFreeMarker("login.ftl");

    }


    public void register() {

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);
        renderFreeMarker("register.ftl");
    }

    @Before(RegisterValidator.class)
    public void doRegister() {

        String name = getPara("name");
        String password = getPara("password");
        String gender = getPara("gender");
        String address = getPara("address");
        String tel = getPara("tel");
        String ID = getPara("ID");
        int stu_class = getParaToInt("class");

        Student student = new Student();
        student.setName(name);
        student.setPassword(password);
        student.setAddress(address);
        student.setTel(tel);
        student.setGender(gender);
        student.setID(ID);
        student.setStatus(0);
        student.setAvatar("template/image/avatar.png");
        student.setClassNumber(stu_class);

        boolean success = false;
        try {
            student.save();
            success = true;
        } catch (Exception e) {
            LogKit.error("用户注册失败，原因是：" + e.getMessage());
        }
        String message = success ? "注册成功" : "注册失败";
        Kv result = Kv.by("success", success).set("message", message);
        renderJson(result);

        // renderFreeMarker("register-result.ftl");
    }

    public void login() {
        renderFreeMarker("login.ftl");
    }

    @Before(LoginValidator.class)
    public void loginCheck() {
        String username = getPara("username");
        String password = getPara("password");
//        String passwordMd5= HashKit.md5(password);
        //注释掉的是另一种查询方法
        //String sql = "SELECT * FROM t_user WHERE username = ? AND password = ?";
        String sql_stu = Db.getSql("checkLoginUser_stu");
        String sql_admin = Db.getSql("checkLoginUser_admin");

        //SqlPara sqlpara = Db.getSqlPara("checkLoginUser");
        //sqlpara.addPara(username);
        //sqlpara.addPara(password);
        //User.dao.find(sqlpara);
        List<Student> students = Student.dao.find(sql_stu, username, password);
        List<Admin> admins = Admin.dao.find(sql_admin, username, password);
        boolean success = false;
        int flag = 0;
        //登录成功
        if (students.size() != 0) {
            //跳转到主页面
//            redirect("/main");
            //往session中放入一个key为username的变量，值为用户登录的用户名

            setSessionAttr("user", students.get(0));
//            setAttr("student",students.get(0));
            success = true;
            flag = 0;
        } else {
            if (admins.size() != 0) {
                setSessionAttr("user", admins.get(0));
//              setAttr("admins",admins.get(0));
                success = true;
                flag = 1;
            } else {
                //登录失败
                setAttr("errmsg", "用户名或密码错误");
                // renderFreeMarker("login.ftl");
            }

        }
        String message = success ? "登录成功" : "登录失败,密码或者用户名错误";
        Kv result = Kv.by("success", success).set("message", message).set("flag", flag);
        renderJson(result);
    }

    public void logout() {
        removeSessionAttr("users");
        redirect("/login");
    }


}
