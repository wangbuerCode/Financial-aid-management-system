package controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.Kv;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.upload.UploadFile;
import interceptor.Login;
import model.Admin;
import model.Apply;
import model.Class;
import model.Student;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Before(Login.class)
public class StudentController extends Controller {

    public void grantInfo(){

        //以下是分页操作
        Student user = getSessionAttr("user");
        System.out.println("idwei"+user.getStuId());
        SqlPara sqlPara = Db.getSqlPara("StuGrantInfo",user.getStuId());
        Integer pageNumber = getParaToInt("page", 1);
        Page<Apply> page = Apply.dao.paginate(pageNumber, 10, sqlPara);
        setAttr("page", page);



        renderFreeMarker("grantInfo.ftl");

    }


    public void logout() {
        removeSessionAttr("users");
        redirect("/login");
    }

    public void delete_info() {
        String stu_info_sql = Db.getSql("search_stu_info");
        Student student = getSessionAttr("user");
        List<Student> students = Student.dao.find(stu_info_sql, student.getStuId());
        setAttr("student", students.get(0));

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);

        Student temp = getSessionAttr("user");


        Integer applyid = getParaToInt(0, 1);
        boolean success = false;
        try {
            Db.delete("DELETE FROM t_apply where t_apply.applyId=?", applyid);
            success = true;
        } catch (Exception e) {
            LogKit.error("删除失败，原因是：" + e.getMessage());
        }

        //下列语句判断是不是该同学申请过助学金
        List<Apply> applies = Apply.dao.find("\n" +
                "\tSELECT\n" +
                "\t*\n" +
                "\tFROM\n" +
                "\tt_apply\n" +
                "\tWHERE\n" +
                "\tstuId = ?", temp.getStuId());

        setAttr("applies", applies);

        String message = success ? "删除成功" : "删除失败";
        Kv result = Kv.by("success", success).set("message", message);
        renderJson(result);

        renderFreeMarker("totalApply.ftl");

    }

    public void modify_applyinfo() {

//        Integer applyid = getParaToInt(0,1);
        Integer applyid = getParaToInt("applyId");
        String name = getPara("name");
        int stuId = getParaToInt("stuId");
        String id = getPara("ID");
        String tel = getPara("tel");
        int isDibao = getParaToInt("IsDibao");
        int isJiandang = getParaToInt("IsJiandang");

        String fatherName = getPara("fatherName");
        String fatherTel = getPara("fatherTel");
        String fatherJob = getPara("fatherJob");
        String motherName = getPara("motherName");
        String motherTel = getPara("motherTel");
        String motherJob = getPara("motherJob");
        String other1Name = getPara("other1Name");
        String other1Tel = getPara("other1Tel");
        String other1Job = getPara("other1Job");
        String other2Name = getPara("other2Name");
        String other2Tel = getPara("other2Tel");
        String other2Job = getPara("other2Job");

        int familyincome = getParaToInt("Familyincome");
        String familyAddress = getPara("familyAddress");
        int grantLevel = getParaToInt("grantLevel");
        String applyReason = getPara("applyReason");

        String tel1 = getPara("tel");

        Apply apply = new Apply().setStuId(stuId).setStatus(0).setGrantLevel(grantLevel).setIsDibao(isDibao).setIsJiandang(isJiandang);
        apply.setFamilyAddress(familyAddress);
        apply.setFamilyincome(familyincome);
        apply.setApplyReason(applyReason);
        apply.setAttachment("NULL");
        apply.setID(id);
        apply.setFamilyInfo("null");
        apply.setFatherName(fatherName).setFatherTel(fatherTel).setFatherJob(fatherJob);
        apply.setMotherName(motherName).setMotherTel(motherTel).setMotherJob(motherJob);
        apply.setOther1Name(other1Name).setOther1Tel(other1Tel).setOther1Job(other1Job);
        apply.setOther2Name(other2Name).setOther2Tel(other2Tel).setOther2Job(other2Job);
        apply.setApplyId(applyid);

        boolean success = false;
        try {
            Db.update("update t_student set tel=? where stuId= ?", tel, stuId);
            Db.delete("DELETE FROM t_apply where t_apply.applyId=?", applyid);
            apply.save();
            success = true;

        } catch (Exception e) {
            LogKit.error("修改失败，原因是：" + e.getMessage());
        }

        String message = success ? "修改成功" : "修改失败";
        Kv result = Kv.by("success", success).set("message", message);
        System.out.print(success);
        renderJson(result);
    }

    public void modify_info() {

        Integer applyid = getParaToInt(0, 1);
        Apply apply = Apply.dao.findById(applyid);
        Student student = Student.dao.findById(apply.getStuId());
        setAttr("student", student);
        setAttr("apply", apply);
        renderFreeMarker("modify_info.ftl");


    }

    private static final List<String> relative_names = Arrays.asList("父亲", "母亲", "兄弟", "姐妹");

    public void search_info() {
        String stu_info_sql = Db.getSql("search_stu_info");
        Student student = getSessionAttr("user");
        List<Student> students = Student.dao.find(stu_info_sql, student.getStuId());
        setAttr("student", students.get(0));

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);

        Student temp = getSessionAttr("user");
        //下列语句判断是不是该同学申请过助学金
        String search_apply = Db.getSql("search_apply");
        List<Apply> applies = Apply.dao.find(search_apply, temp.getStuId());

        setAttr("apply", applies.get(0));

        renderFreeMarker("search_info.ftl");

    }

    public void totalApply() {

        String stu_info_sql = Db.getSql("search_stu_info");
        Student student = getSessionAttr("user");
        List<Student> students = Student.dao.find(stu_info_sql, student.getStuId());
        setAttr("student", students.get(0));

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);

        Student temp = getSessionAttr("user");
        //下列语句判断是不是该同学申请过助学金
        List<Apply> applies = Apply.dao.find("\n" +
                "\tSELECT\n" +
                "\t*\n" +
                "\tFROM\n" +
                "\tt_apply\n" +
                "\tWHERE\n" +
                "\tstuId = ?", temp.getStuId());

        setAttr("applies", applies);
        System.out.println("applies长度" + applies.size());

        renderFreeMarker("totalApply.ftl");

    }

    public void modify_stu_avatar() {

        String stu_info_sql = Db.getSql("search_stu_info");
        Student student = getSessionAttr("user");
        List<Student> students = Student.dao.find(stu_info_sql, student.getStuId());
        setAttr("student", students.get(0));
        renderFreeMarker("modify_stu_avatar.ftl");
    }

    public void applyMoney() {

        String stu_info_sql = Db.getSql("search_stu_info");
        Student student = getSessionAttr("user");
        List<Student> students = Student.dao.find(stu_info_sql, student.getStuId());
        setAttr("student", students.get(0));

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);

        Student temp = getSessionAttr("user");

        //下列语句判断是不是该同学申请过助学金

        List<Apply> applies = Apply.dao.find("\n" +
                "\tSELECT\n" +
                "\t*\n" +
                "\tFROM\n" +
                "\tt_apply\n" +
                "\tWHERE\n" +
                "\tstuId = ?", temp.getStuId());

        setAttr("applies", applies);

        renderFreeMarker("applyMoney.ftl");
    }

    public void doapplyMoney() {
        String name = getPara("name");
        int stuId = getParaToInt("stuId");
        String id = getPara("ID");
        String tel = getPara("tel");
        int isDibao = getParaToInt("IsDibao");
        int isJiandang = getParaToInt("IsJiandang");

        String fatherName = getPara("fatherName");
        String fatherTel = getPara("fatherTel");
        String fatherJob = getPara("fatherJob");
        String motherName = getPara("motherName");
        String motherTel = getPara("motherTel");
        String motherJob = getPara("motherJob");
        String other1Name = getPara("other1Name");
        String other1Tel = getPara("other1Tel");
        String other1Job = getPara("other1Job");
        String other2Name = getPara("other2Name");
        String other2Tel = getPara("other2Tel");
        String other2Job = getPara("other2Job");

        int familyincome = getParaToInt("Familyincome");
        String familyAddress = getPara("familyAddress");
        int grantLevel = getParaToInt("grantLevel");
        String applyReason = getPara("applyReason");

        String Familyinfo = fatherName + "/" + fatherTel + "/" + fatherJob + "/" + motherName + "/" + motherTel + "/" + motherJob + "/" + other1Name + "/" + other1Tel + "/"
                + other1Job + "/" + other2Name + "/" + other2Tel + "/" + other2Job;

        Apply apply = new Apply().setStuId(stuId).setStatus(0).setGrantLevel(grantLevel).setIsDibao(isDibao).setIsJiandang(isJiandang);
        apply.setFamilyAddress(familyAddress);
        apply.setFamilyincome(familyincome);
        apply.setApplyReason(applyReason);
        apply.setAttachment("NULL");
        apply.setID(id);
        apply.setFamilyInfo("null");
        apply.setFatherName(fatherName).setFatherTel(fatherTel).setFatherJob(fatherJob);
        apply.setMotherName(motherName).setMotherTel(motherTel).setMotherJob(motherJob);
        apply.setOther1Name(other1Name).setOther1Tel(other1Tel).setOther1Job(other1Job);
        apply.setOther2Name(other2Name).setOther2Tel(other2Tel).setOther2Job(other2Job);
        boolean success = false;
        try {
            apply.save();
            success = true;

        } catch (Exception e) {
            LogKit.error("申请失败，原因是：" + e.getMessage());
        }

        String message = success ? "申请成功" : "申请失败";
        Kv result = Kv.by("success", success).set("message", message);
        System.out.print(success);
        renderJson(result);


    }

    private static final List<String> ALLOW_IMAGE_TYPES = Arrays.asList("image/jpeg", "image/png", "image.jpg");

    public void upload() {
        Boolean success = false;
        UploadFile upload = this.getFile();
        String fileName = upload.getOriginalFileName();
//      UploadFile upFile = getFile("file", "", maxSize, "utf-8");// maxsize限制上传文件的大小，也可以在配置文件中设置

        File file = upload.getFile();
        String contentType = upload.getContentType();
        if (ALLOW_IMAGE_TYPES.contains(contentType)) {
            System.out.println("文件类型为" + contentType);

            String webRootPath = PathKit.getWebRootPath();//得到web路径

            PropKit.use("myconfig.properties");//从配置文件中读取保存路径
//            String saveFilePathforimage = PropKit.get("saveFilePathforimage");
            String  saveFilePathforimage=webRootPath+"\\template\\img\\";
            String savaFileName = UUID.randomUUID().toString() + ".png";

            System.out.println("保存路径=" + saveFilePathforimage);
            String saveNme = saveFilePathforimage + savaFileName;

            String mysql_save_Path = "/template/img/" + savaFileName;
            System.out.println("保存路径=" + saveNme);

            File Direction = new File(saveFilePathforimage);
            //判断文件夹是否存在 如果不存在 就创建文件夹
            if (!Direction.exists()) {
                Direction.mkdirs();
            }

            File t = new File(saveNme);
            try {
                t.createNewFile();
                Student student = getSessionAttr("user");
                //D:\2018.6.9\src\main\webapp\img\aef5b341-3cdb-4331-b2f5-8d6fa99ce4e7
                Db.update("update t_student set avatar =? where stuId=? ", mysql_save_Path, student.getStuId());
                success = true;
            } catch (Exception e) {
                e.printStackTrace();
                LogKit.error("上传失败，原因是：" + e.getMessage());
            }
            fileChannelCopy(file, t);
            file.delete();
            this.renderHtml("上传成功");

        } else {
            file.delete();
            LogKit.error("上传失败，原因是 上传的不是图片");
            System.out.println("文件类型为" + contentType);
        }

        String stu_info_sql = Db.getSql("search_stu_info");
        Student student = getSessionAttr("user");
        List<Student> students = Student.dao.find(stu_info_sql, student.getStuId());
        setAttr("student", students.get(0));

        String message = success ? "上传成功" : "上传失败";
        Kv result = Kv.by("success", success).set("message", message);
        renderJson(result);
    }

    public void fileChannelCopy(File s, File t) {
        FileInputStream fi = null;
        FileOutputStream fo = null;
        FileChannel in = null;
        FileChannel out = null;
        try {
            fi = new FileInputStream(s);
            fo = new FileOutputStream(t);
            in = fi.getChannel();//得到对应的文件通道
            out = fo.getChannel();
            in.transferTo(0, in.size(), out);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fi.close();
                in.close();
                fo.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void index() {
        String stu_info_sql = Db.getSql("search_stu_info");
        Student student = getSessionAttr("user");
        List<Student> students = Student.dao.find(stu_info_sql, student.getStuId());
        setAttr("student", students.get(0));
        renderFreeMarker("index.ftl");
    }

    public void modify_stu_info() {

        String stu_info_sql = Db.getSql("search_stu_info");
        Student student = getSessionAttr("user");
        List<Student> students = Student.dao.find(stu_info_sql, student.getStuId());
        setAttr("student", students.get(0));

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);
        renderFreeMarker("modify_stu_info.ftl");
    }

    public void modify_stu_info_save() {
        int stuId = getParaToInt("stuId", 0);

        String name = getPara("username");
        String password = getPara("password");
        String gender = getPara("gender");
        String address = getPara("address");
        String tel = getPara("tel");
        String ID = getPara("ID");
        int stu_class = getParaToInt("class");
        Student byId = Student.dao.findById(stuId);
        String avatar = byId.getAvatar();

        LogKit.debug("avatar路径=" + avatar);

        Student student = new Student();
        student.setStuId(stuId);
        student.setName(name);
        student.setPassword(password);
        student.setAddress(address);
        student.setTel(tel);
        student.setGender(gender);
        student.setID(ID);
        student.setStatus(0);
        student.setAvatar(avatar);
        student.setClassNumber(stu_class);
        boolean success = false;
        try {
            student.update();
            success = true;

        } catch (Exception e) {
            LogKit.error("修改失败，原因是：" + e.getMessage());
        }

        String message = success ? "修改成功" : "修改失败";
        Kv result = Kv.by("success", success).set("message", message);
        System.out.print(success);
        renderJson(result);

    }
}
