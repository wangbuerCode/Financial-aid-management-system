package controller;

import com.jfinal.aop.Before;

import com.jfinal.core.Controller;
import com.jfinal.kit.Kv;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.upload.UploadFile;
import interceptor.Login;
import model.*;
import model.Class;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Before(Login.class)
public class AdminController extends Controller {
    public void admin_grant_way(){
        Integer wayid = getParaToInt(0, 1);

        if(wayid==0){
            SqlPara sqlPara = Db.getSqlPara("adminFindGrantinfo_0");
            Integer pageNumber = getParaToInt("page", 1);
            Page<Apply> page = Apply.dao.paginate(pageNumber, 3, sqlPara);
            setAttr("page", page);
        }else if(wayid==1){
            SqlPara sqlPara = Db.getSqlPara("adminFindGrantinfo_1");
            Integer pageNumber = getParaToInt("page", 1);
            Page<Apply> page = Apply.dao.paginate(pageNumber, 3, sqlPara);
            setAttr("page", page);
        }
        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        renderFreeMarker("grantInfo.ftl");
    }

    public void  grantPass(){
        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        Integer grantId = getParaToInt(0, 1);
        Grant grant = Grant.dao.findById(grantId);
        Db.update("UPDATE t_grant set `grantStatus`=1 WHERE grantId=?",grantId);


        //以下是分页操作
        SqlPara sqlPara = Db.getSqlPara("grantInfo");
        Integer pageNumber = getParaToInt("page", 1);
        Page<Apply> page = Apply.dao.paginate(pageNumber, 3, sqlPara);
        setAttr("page", page);


        renderFreeMarker("grantInfo.ftl");
    }

    public void grantInfo(){
        //以下是分页操作
        SqlPara sqlPara = Db.getSqlPara("grantInfo");
        Integer pageNumber = getParaToInt("page", 1);
        Page<Apply> page = Apply.dao.paginate(pageNumber, 10, sqlPara);
        setAttr("page", page);

        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        renderFreeMarker("grantInfo.ftl");

    }

    public void admin_way(){
        Integer wayid = getParaToInt(0, 1);
        System.out.println("查询shenqing方式"+wayid);
        if(wayid==0){
            SqlPara sqlPara = Db.getSqlPara("adminFindApplyinfo_0");
            Integer pageNumber = getParaToInt("page", 1);

            Page<Apply> page = Apply.dao.paginate(pageNumber, 3, sqlPara);
            setAttr("page", page);
        }else if(wayid==1){
            SqlPara sqlPara = Db.getSqlPara("adminFindApplyinfo_1");
            Integer pageNumber = getParaToInt("page", 1);
            Page<Apply> page = Apply.dao.paginate(pageNumber, 3, sqlPara);
            setAttr("page", page);
        }else if (wayid==2){
            SqlPara sqlPara = Db.getSqlPara("adminFindApplyinfo_2");
            Integer pageNumber = getParaToInt("page", 1);
            Page<Apply> page = Apply.dao.paginate(pageNumber, 3, sqlPara);
            setAttr("page", page);
        }
        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        renderFreeMarker("adminFindApplyinfo.ftl");
    }

    public void refuse(){
        Integer applyid = getParaToInt(0, 1);

        Db.update("UPDATE t_apply set `status`=2 WHERE applyId=?",applyid);

        Db.delete("DELETE FROM t_grant where t_grant.applyId=?", applyid);

        //以下是分页操作
        SqlPara sqlPara = Db.getSqlPara("adminFindApplyinfo");
        Integer pageNumber = getParaToInt("page", 1);
        Page<Apply> page = Apply.dao.paginate(pageNumber, 3, sqlPara);
        setAttr("page", page);

        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        renderFreeMarker("adminFindApplyinfo.ftl");

    }
    public void pass(){
        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        Integer applyid = getParaToInt(0, 1);
        Apply apply = Apply.dao.findById(applyid);
        Db.update("UPDATE t_apply set `status`=1 WHERE applyId=?",applyid);




        Date date=new Date();
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        String nyr = dateFormat.format(date);

        Grant grant = new Grant().setAdminId(admin1.getAdminId()).setGrantStatus(0).setStuId(apply.getStuId()).setTime(nyr).setApplyId(applyid);
        System.out.println("日期"+date);
        grant.save();


        //以下是分页操作
        SqlPara sqlPara = Db.getSqlPara("adminFindApplyinfo");
        Integer pageNumber = getParaToInt("page", 1);
        Page<Apply> page = Apply.dao.paginate(pageNumber, 3, sqlPara);
        setAttr("page", page);



        renderFreeMarker("adminFindApplyinfo.ftl");

    }

    public void admin_apply_stuinfo() {

        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        Integer applyid = getParaToInt(0, 1);

        String adminFindApplyById = Db.getSql("adminFindApplyById");
        List<Apply> applies = Apply.dao.find(adminFindApplyById,applyid);
        setAttr("apply", applies.get(0));
        renderFreeMarker("admin_apply_stuinfo.ftl");

    }

    public void applyInfo(){
        //以下是分页操作
        SqlPara sqlPara = Db.getSqlPara("adminFindApplyinfo");
        Integer pageNumber = getParaToInt("page", 1);
        Page<Apply> page = Apply.dao.paginate(pageNumber, 3, sqlPara);
        setAttr("page", page);

        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        renderFreeMarker("adminFindApplyinfo.ftl");

    }

    public void  DoAddAdmin(){
        String name = getPara("username");
        String password = getPara("password");
        String gender = getPara("gender");
        String address = getPara("address");
        String tel = getPara("tel");
        int power = getParaToInt("power");
        int stu_class = getParaToInt("class");
        String avatar = "/template/img/72b77be0-c4ca-4b7e-9965-60924636599a.png";

        Admin admin = new Admin();

        admin.setName(name);
        admin.setPassword(password);
        admin.setAddress(address);
        admin.setTel(tel);
        admin.setGender(gender);
        admin.setPower(power);
        admin.setStatus(0);
        admin.setAvatar(avatar);
        admin.setClassNumber(stu_class);
        boolean success = false;
        try {
            admin.save();
            success = true;

        } catch (Exception e) {
            LogKit.error("添加失败，原因是：" + e.getMessage());
        }

        String message = success ? "添加成功" : "添加失败";
        Kv result = Kv.by("success", success).set("message", message);
        System.out.print(success);
        renderJson(result);
    }

    public void addAdminInfo(){

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);

        String search_admin_info = Db.getSql("search_admin_info");
        Admin admin = getSessionAttr("user");
        List<Admin> admins = Admin.dao.find(search_admin_info, admin.getAdminId());
        setAttr("admin", admins.get(0));

        renderFreeMarker("addAdmintInfo.ftl");

    }

    public void  AdminInfoBySearch(){
        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin);

        if(select==0){
            System.out.println("按照姓名搜寻");
            String findAdminByName = Db.getSql("findAdminByName");
            List<Admin> adminList = Admin.dao.find(findAdminByName,searchstring);
            setAttr("adminList",adminList);

        }else if(select==1){

            String findAdminById = Db.getSql("findAdminById");
            List<Admin> adminList = Admin.dao.find(findAdminById, searchint);
            setAttr("adminList",adminList);
        }

        renderFreeMarker("AdminInfoBySearch.ftl");
    }

    public void searchAdmin(){
        select = getParaToInt("select",-1);
        System.out.println("\n"+" 搜索方式"+select);

        if(select==0){
            searchstring = getPara("search");
            System.out.println("按照姓名搜寻");
        }else if(select==1){
            searchint = getParaToInt("search");
        }

        boolean success=true;
        String message = success ? "删除成功" : "删除失败";
        Kv result = Kv.by("success", success).set("message", message);
        renderJson(result);
    }


    public void delete_admininfo(){
        Integer adminid = getParaToInt(0, 1);

        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        try {
            Db.delete("DELETE FROM t_admin where t_admin.admin_id=?", adminid);
        } catch (Exception e) {
            LogKit.error("删除失败，原因是：" + e.getMessage());
        }
        redirect("/admin/AdminInfo");
    }

    public  void admin_modify_admin_info_save(){
        int adminId = getParaToInt("adminId", 0);
        String name = getPara("username");
        String password = getPara("password");
        String gender = getPara("gender");
        String address = getPara("address");
        String tel = getPara("tel");
        int power = getParaToInt("power");
        int stu_class = getParaToInt("class");

        Admin admin1 = Admin.dao.findById(adminId);
        String avatar = admin1.getAvatar();

        Admin admintemp = new Admin();
        admintemp.setAdminId(adminId);
        admintemp.setName(name);
        admintemp.setPassword(password);
        admintemp.setAddress(address);
        admintemp.setTel(tel);
        admintemp.setGender(gender);
        admintemp.setPower(power);
        admintemp.setStatus(0);
        admintemp.setAvatar(avatar);
        admintemp.setClassNumber(stu_class);
        boolean success = false;
        try {
            admintemp.update();
            success = true;

        } catch (Exception e) {
            LogKit.error("修改失败，原因是：" + e.getMessage());
        }

        String message = success ? "修改成功" : "修改失败";
        Kv result = Kv.by("success", success).set("message", message);
        System.out.print(success);
        renderJson(result);
    }

    public void modify_admininfo(){

        Integer stuid = getParaToInt(0, 1);
        Admin admin2 = Admin.dao.findById(stuid);
        setAttr("adminuser", admin2);

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);

        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        renderFreeMarker("admin_modifyAdmin_info.ftl");
    }
    public void AdminInfo(){
        //以下是分页操作
        SqlPara sqlPara = Db.getSqlPara("findlAllUser");
        Integer pageNumber = getParaToInt("page", 1);
        Page<Admin> page = Admin.dao.paginate(pageNumber, 10, sqlPara);
        setAttr("page", page);

        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        renderFreeMarker("AdminInfo.ftl");
    }

    public void DoAddStu(){
        String name = getPara("username");
        String password = getPara("password");
        String gender = getPara("gender");
        String address = getPara("address");
        String tel = getPara("tel");
        String ID = getPara("ID");
        int stu_class = getParaToInt("class");
        String avatar = "/template/img/72b77be0-c4ca-4b7e-9965-60924636599a.png";

        Student student = new Student();

        student.setName(name);
        student.setPassword(password);
        student.setAddress(address);
        student.setTel(tel);
        student.setGender(gender);
        student.setID(ID);
        student.setStatus(0);
        student.setAvatar(avatar);
        student.setClassNumber(stu_class);
        boolean success = false;
        try {
            student.save();
            success = true;

        } catch (Exception e) {
            LogKit.error("添加失败，原因是：" + e.getMessage());
        }

        String message = success ? "添加成功" : "添加失败";
        Kv result = Kv.by("success", success).set("message", message);
        System.out.print(success);
        renderJson(result);
    }

    public void addStudentInfo(){

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);

        String search_admin_info = Db.getSql("search_admin_info");
        Admin admin = getSessionAttr("user");
        List<Admin> admins = Admin.dao.find(search_admin_info, admin.getAdminId());
        setAttr("admin", admins.get(0));
        renderFreeMarker("addStudentInfo.ftl");

    }

    private static Integer select=-1;
    private static String searchstring=null;
    private static Integer searchint=-1;

    public void StudentInfoBySearch(){

        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin);

        if(select==0){

            System.out.println("按照姓名搜寻");
            String findStuByName = Db.getSql("findStuByName");
            List<Student> students = Student.dao.find(findStuByName,searchstring);
            setAttr("students",students);

        }else if(select==1){

            String findStuByStuid = Db.getSql("findStuByStuid");
            List<Student> students = Student.dao.find(findStuByStuid, searchint);
            setAttr("students",students);

        }

        renderFreeMarker("StudentInfoBySearch.ftl");
    }
    public void searchStu(){

        select = getParaToInt("select",-1);
        System.out.println("\n"+" 搜索方式"+select);

        if(select==0){
            searchstring = getPara("search");
            System.out.println("按照姓名搜寻");
        }else if(select==1){
            searchint = getParaToInt("search");
        }

        boolean success=true;
        String message = success ? "删除成功" : "删除失败";
        Kv result = Kv.by("success", success).set("message", message);
        renderJson(result);
    }

    public void delete_info() {

        Integer stuid = getParaToInt(0, 1);


        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        try {
            Db.delete("DELETE FROM t_student where t_student.stuId=?", stuid);
        } catch (Exception e) {
            LogKit.error("删除失败，原因是：" + e.getMessage());
        }
        redirect("/admin/StudentInfo");
    }


    public void admin_modify_stu_info_save(){
        int stuId = getParaToInt("stuId", 0);
        String name = getPara("username");
        String password = getPara("password");
        String gender = getPara("gender");
        String address = getPara("address");
        String tel = getPara("tel");
        String ID = getPara("ID");
        int stu_class = getParaToInt("class");

        Student student1 = Student.dao.findById(stuId);
        String avatar = student1.getAvatar();

        Student student = new Student();
        student.setStuId(stuId);
        student.setName(name);
        student.setPassword(password);
        student.setAddress(address);
        student.setTel(tel);
        student.setGender(gender);
        student.setID(ID);
        student.setStatus(0);
        student.setAvatar(avatar);
        student.setClassNumber(stu_class);
        boolean success = false;
        try {
            student.update();
            success = true;

        } catch (Exception e) {
            LogKit.error("修改失败，原因是：" + e.getMessage());
        }

        String message = success ? "修改成功" : "修改失败";
        Kv result = Kv.by("success", success).set("message", message);
        System.out.print(success);
        renderJson(result);
    }

    public void modify_info(){

        Integer stuid = getParaToInt(0, 1);
        Student student = Student.dao.findById(stuid);
        setAttr("student", student);

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);

        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        renderFreeMarker("admin_modify_info.ftl");
    }
    public void logout() {
        removeSessionAttr("users");
        redirect("/login");
    }
    public  void StudentInfo(){

        //以下是分页操作
        SqlPara sqlPara = Db.getSqlPara("findAllStu");
        Integer pageNumber = getParaToInt("page", 1);
        Page<Student> page = Student.dao.paginate(pageNumber, 10, sqlPara);
        setAttr("page", page);

        Admin admin = getSessionAttr("user");
        Admin admin1 = Admin.dao.findById(admin.getAdminId());
        setAttr("admin",admin1);

        renderFreeMarker("StudentInfo.ftl");

    }

    public void modify_admin_info_save(){
        int adminid = getParaToInt("adminid", 0);
        String name = getPara("username");
        String password = getPara("password");
        String gender = getPara("gender");
        String address = getPara("address");
        String tel = getPara("tel");
//        int power = getParaToInt("power");
        int classNumber = getParaToInt("class");
        String modify_admin_info = Db.getSql("modify_admin_info");
        boolean success = false;
        try {
            Db.update(modify_admin_info,name,password,address,tel,gender,classNumber,adminid);
            success = true;

        } catch (Exception e) {
            LogKit.error("修改失败，原因是：" + e.getMessage());
        }
        String message = success ? "修改成功" : "修改失败";
        Kv result = Kv.by("success", success).set("message", message);
        System.out.print(success);
        renderJson(result);
    }

    public void modify_admin_info() {

        String search_admin_info = Db.getSql("search_admin_info");
        Admin admin = getSessionAttr("user");
        List<Admin> admins = Admin.dao.find(search_admin_info, admin.getAdminId());
        setAttr("admin", admins.get(0));

        SqlPara sqlPara = Db.getSqlPara("find_class");
        List<Class> classes = Class.dao.find(sqlPara);
        setAttr("classes", classes);

        renderFreeMarker("modify_admin_info.ftl");
    }

    private static final List<String> ALLOW_IMAGE_TYPES = Arrays.asList("image/jpeg", "image/png", "image.jpg");
    public void upload() {
        Boolean success = false;
        UploadFile upload = this.getFile();
        String fileName = upload.getOriginalFileName();
//      UploadFile upFile = getFile("file", "", maxSize, "utf-8");// maxsize限制上传文件的大小，也可以在配置文件中设置

        File file = upload.getFile();
        String contentType = upload.getContentType();
        if (ALLOW_IMAGE_TYPES.contains(contentType)) {
            System.out.println("文件类型为" + contentType);

            String webRootPath = PathKit.getWebRootPath();//得到web路径

            System.out.println("到web路径="+webRootPath);
//
//            PropKit.use("myconfig.properties");//从配置文件中读取保存路径
//            String saveFilePathforimage = PropKit.get("saveFilePathforimage");

            String savaFileName = UUID.randomUUID().toString() + ".png";
            //saveFilePathforimage=F:\\StuApplyGrant\\src\\main\\webapp\\template\\img\\
//            webRootPath =F:\StuApplyGrant\src\main\webapp
            String  saveFilePathforimage=webRootPath+"\\template\\img\\";

            String saveNme = saveFilePathforimage + savaFileName;

            String mysql_save_Path = "/template/img/" + savaFileName;


            File Direction = new File(saveFilePathforimage);
            //判断文件夹是否存在 如果不存在 就创建文件夹
            if (!Direction.exists()) {
                Direction.mkdirs();
            }

            File t = new File(saveNme);
            try {
                t.createNewFile();
                Admin admin = getSessionAttr("user");
                //D:\2018.6.9\src\main\webapp\img\aef5b341-3cdb-4331-b2f5-8d6fa99ce4e7
                Db.update("update t_admin set avatar =? where admin_id=? ", mysql_save_Path, admin.getAdminId());
                success = true;
            } catch (Exception e) {
                e.printStackTrace();
                LogKit.error("上传失败，原因是：" + e.getMessage());
            }
            fileChannelCopy(file, t);
            file.delete();
            this.renderHtml("上传成功");

        } else {
            file.delete();
            LogKit.error("上传失败，原因是 上传的不是图片");
            System.out.println("文件类型为" + contentType);
        }

        String search_admin_info = Db.getSql("search_admin_info");
        Admin admin = getSessionAttr("user");
        List<Admin> admins = Admin.dao.find(search_admin_info, admin.getAdminId());
        setAttr("admin", admins.get(0));
        String message = success ? "上传成功" : "上传失败";
        Kv result = Kv.by("success", success).set("message", message);
        renderJson(result);
    }

    public void fileChannelCopy(File s, File t) {
        FileInputStream fi = null;
        FileOutputStream fo = null;
        FileChannel in = null;
        FileChannel out = null;
        try {
            fi = new FileInputStream(s);
            fo = new FileOutputStream(t);
            in = fi.getChannel();//得到对应的文件通道
            out = fo.getChannel();
            in.transferTo(0, in.size(), out);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fi.close();
                in.close();
                fo.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public  void modify_admin_avatar(){
        String search_admin_info = Db.getSql("search_admin_info");
        Admin admin = getSessionAttr("user");
        List<Admin> admins = Admin.dao.find(search_admin_info, admin.getAdminId());
        setAttr("admin", admins.get(0));
        renderFreeMarker("modify_admin_avatar.ftl");
    }

    public void index(){

        String search_admin_info = Db.getSql("search_admin_info");
        Admin admin = getSessionAttr("user");
        List<Admin> admins = Admin.dao.find(search_admin_info, admin.getAdminId());
        setAttr("admin", admins.get(0));

        renderFreeMarker("index.ftl");
    }

}
