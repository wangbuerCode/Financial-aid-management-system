package model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseApply<M extends BaseApply<M>> extends Model<M> implements IBean {

	public M setApplyId(java.lang.Integer applyId) {
		set("applyId", applyId);
		return (M)this;
	}
	
	public java.lang.Integer getApplyId() {
		return getInt("applyId");
	}

	public M setStuId(java.lang.Integer stuId) {
		set("stuId", stuId);
		return (M)this;
	}
	
	public java.lang.Integer getStuId() {
		return getInt("stuId");
	}

	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}
	
	public java.lang.Integer getStatus() {
		return getInt("status");
	}

	public M setGrantLevel(java.lang.Integer grantLevel) {
		set("grantLevel", grantLevel);
		return (M)this;
	}
	
	public java.lang.Integer getGrantLevel() {
		return getInt("grantLevel");
	}

	public M setIsDibao(java.lang.Integer IsDibao) {
		set("IsDibao", IsDibao);
		return (M)this;
	}
	
	public java.lang.Integer getIsDibao() {
		return getInt("IsDibao");
	}

	public M setIsJiandang(java.lang.Integer IsJiandang) {
		set("IsJiandang", IsJiandang);
		return (M)this;
	}
	
	public java.lang.Integer getIsJiandang() {
		return getInt("IsJiandang");
	}

	public M setFamilyAddress(java.lang.String familyAddress) {
		set("familyAddress", familyAddress);
		return (M)this;
	}
	
	public java.lang.String getFamilyAddress() {
		return getStr("familyAddress");
	}

	public M setFamilyincome(java.lang.Integer Familyincome) {
		set("Familyincome", Familyincome);
		return (M)this;
	}
	
	public java.lang.Integer getFamilyincome() {
		return getInt("Familyincome");
	}

	public M setApplyReason(java.lang.String applyReason) {
		set("applyReason", applyReason);
		return (M)this;
	}
	
	public java.lang.String getApplyReason() {
		return getStr("applyReason");
	}

	public M setAttachment(java.lang.String attachment) {
		set("attachment", attachment);
		return (M)this;
	}
	
	public java.lang.String getAttachment() {
		return getStr("attachment");
	}

	public M setFamilyInfo(java.lang.String FamilyInfo) {
		set("FamilyInfo", FamilyInfo);
		return (M)this;
	}
	
	public java.lang.String getFamilyInfo() {
		return getStr("FamilyInfo");
	}

	public M setID(java.lang.String ID) {
		set("ID", ID);
		return (M)this;
	}
	
	public java.lang.String getID() {
		return getStr("ID");
	}

	public M setFatherName(java.lang.String fatherName) {
		set("fatherName", fatherName);
		return (M)this;
	}
	
	public java.lang.String getFatherName() {
		return getStr("fatherName");
	}

	public M setFatherTel(java.lang.String fatherTel) {
		set("fatherTel", fatherTel);
		return (M)this;
	}
	
	public java.lang.String getFatherTel() {
		return getStr("fatherTel");
	}

	public M setFatherJob(java.lang.String fatherJob) {
		set("fatherJob", fatherJob);
		return (M)this;
	}
	
	public java.lang.String getFatherJob() {
		return getStr("fatherJob");
	}

	public M setMotherName(java.lang.String motherName) {
		set("motherName", motherName);
		return (M)this;
	}
	
	public java.lang.String getMotherName() {
		return getStr("motherName");
	}

	public M setMotherTel(java.lang.String motherTel) {
		set("motherTel", motherTel);
		return (M)this;
	}
	
	public java.lang.String getMotherTel() {
		return getStr("motherTel");
	}

	public M setMotherJob(java.lang.String motherJob) {
		set("motherJob", motherJob);
		return (M)this;
	}
	
	public java.lang.String getMotherJob() {
		return getStr("motherJob");
	}

	public M setOther1Name(java.lang.String other1Name) {
		set("other1Name", other1Name);
		return (M)this;
	}
	
	public java.lang.String getOther1Name() {
		return getStr("other1Name");
	}

	public M setOther1Tel(java.lang.String other1Tel) {
		set("other1Tel", other1Tel);
		return (M)this;
	}
	
	public java.lang.String getOther1Tel() {
		return getStr("other1Tel");
	}

	public M setOther1Job(java.lang.String other1Job) {
		set("other1Job", other1Job);
		return (M)this;
	}
	
	public java.lang.String getOther1Job() {
		return getStr("other1Job");
	}

	public M setOther2Name(java.lang.String other2Name) {
		set("other2Name", other2Name);
		return (M)this;
	}
	
	public java.lang.String getOther2Name() {
		return getStr("other2Name");
	}

	public M setOther2Tel(java.lang.String other2Tel) {
		set("other2Tel", other2Tel);
		return (M)this;
	}
	
	public java.lang.String getOther2Tel() {
		return getStr("other2Tel");
	}

	public M setOther2Job(java.lang.String other2Job) {
		set("other2Job", other2Job);
		return (M)this;
	}
	
	public java.lang.String getOther2Job() {
		return getStr("other2Job");
	}

}
