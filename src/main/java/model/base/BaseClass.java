package model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseClass<M extends BaseClass<M>> extends Model<M> implements IBean {

	public M setClassId(java.lang.Integer classId) {
		set("classId", classId);
		return (M)this;
	}
	
	public java.lang.Integer getClassId() {
		return getInt("classId");
	}

	public M setClassName(java.lang.String className) {
		set("className", className);
		return (M)this;
	}
	
	public java.lang.String getClassName() {
		return getStr("className");
	}

}
