<#-- @ftlvariable name="student" type="model.Student" -->
<#-- @ftlvariable name="student" type="java.util.List<model.Student>" -->
<#--	如果传过来的是一个modeel类，就写成第一种形式，如果传过来的是一个List列表，则写成第二种形式-->

<@override name="title">学生首页</@override>

<@override name="content">

	<div class="ui center aligned grid">
		<div class="row"></div>
		<div class="row"></div>
		<div class="row">
			<div class=" five wide column">
				<img class="ui  image" src="${student.avatar}" title="" style="" style=""x="0" y="0" width="570" height="570">
                <#--                        <img class="ui center aligned  image" src="/template/image/avatar.png" title="" style="">居中显示-->
				<div class="ui button blue  segment ">学号： ${student.stuId} </div>

			</div>

            <#--  注意使用了middle aligned 就上下居中了
			水平对齐用align="center"
			垂直对齐用Valign="middle"
			-->
			<div class="ten wide  celled column middle aligned ">

				<table class="ui  red table">
					<thead>
					<tr>
						<th class="six wide"></th>
						<th class="ten wide"></th>
					</tr>
					</thead>

					<tbody>
					<tr>
						<td>姓名</td>
						<td>${student.name}</td>
					</tr>
					<tr>
						<td>性别</td>
						<td>${student.gender}</td>
					</tr>
					<tr>
						<td>身份证号码</td>
						<td>${student.ID}</td>
					</tr>
					<tr>
						<td>电话号码</td>
						<td>${student.tel}</td>
					</tr>
					<tr>
						<td>班级</td>
						<td>${student.className}</td>
					</tr>
					<tr>
						<td>地址</td>
						<td>${student.address}</td>
					</tr>
					<tr>
						<td>密码</td>
						<td>${student.password}</td>
					</tr>
					</tbody>

					<tfoot class="full-width">
					<tr>
						<th>
							<a class="ui middle aligned small primary  button" href="/student/modify_stu_avatar">
								<i class="user icon"></i> 修改个人头像
							</a>
						</th>
						<th colspan="4">
							<a class="ui middle aligned small primary  button" href="/student/modify_stu_info">
								<i class="user icon"></i> 修改个人信息
							</a>

						</th>
					</tr>
					</tfoot>
				</table>

			</div>
		</div>
	</div>

</@override>


<@extends name="../stu_layout.ftl"></@extends>