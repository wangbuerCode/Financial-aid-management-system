
<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="${base}/scripts/jquery.min.js"></script>
    <script src="${base}/scripts/semantic.min.js"></script>
    <link rel="stylesheet" href="${base}/styles/semantic.min.css"/>
    <title>学生主页</title>

    <script>
        $(document).ready(function() {
            // 鼠标放到 dropdown 时显示下拉菜单，默认只有点击后才显示
            $('.dropdown.item').dropdown({
                on: 'hover'
            });
        });
    </script>

</head>
<body>

<div class="ui container segment padded">
    <div class="ui red massive  inverted menu top fixed borderless stackable ">
        <div class="img"><img src="${base}/template/image/logo.png"></div>
        <div class="item " style="font-size: 24px;color: white;font-family: 微软雅黑">宁夏大学助学金申请系统(学生服务)</div>

        <div class="right menu ">
            <div class="ui dropdown item">
<#--                <img class="ui avatar image" src="../images/wireframe/square-image.png" title="">-->
                <i class="user icon"></i>
                <#if (session.user)??>
                    <div class="text"> 欢迎${session.user.name}同学 </div>
                </#if>
                <i class="icon dropdown"></i>
                <div class="menu">
                    <#if (session.user)??>
                        <a href="${base}/logout" class="item">退出</a>
                    <#else>
                        <a href="${base}/register" class="item">注册</a>
                        <a href="${base}/login" class="item">登录</a>
                    </#if>

                </div>
            </div>

        </div>

    </div>
    </div>



    <div class="ui bottom attached segment pushable segment " style="top: 20px">
        <div class="ui visible inverted left vertical sidebar menu">
            <a class="item" href="${base}/student">
                <i class="home icon"></i>
                个人信息
            </a>
            <a class="item">
                <i class="block layout icon"></i>
                申请助学金
            </a>
            <a class="item">
                <i class="smile icon"></i>
                申请信息汇总
            </a>

        </div>

        <div class="pusher">
<#--            <div class="ui img"><img src="/template/image/avatar.png" alt=""></div>-->

<#--            <div class="ui segment">-->
<#--                <img class="ui centered medium image" src="/template/image/avatar.png" title="" style="">-->
<#--                <p>Te eum doming eirmod, nominati pertinacia argumentum ad his. Ex eam alia facete scriptorem, est autem aliquip detraxit at. Usu ocurreret referrentur at, cu epicurei appellantur vix. Cum ea laoreet recteque electram, eos choro alterum definiebas in. Vim dolorum definiebas an. Mei ex natum rebum iisque.</p>-->
<#--                <p>Audiam quaerendum eu sea, pro omittam definiebas ex. Te est latine definitiones. Quot wisi nulla ex duo. Vis sint solet expetenda ne, his te phaedrum referrentur consectetuer. Id vix fabulas oporteat, ei quo vide phaedrum, vim vivendum maiestatis in.</p>-->
<#--                <img class="ui small centered image" src="/template/image/avatar.png">-->
<#--                <p>Eu quo homero blandit intellegebat. Incorrupte consequuntur mei id. Mei ut facer dolores adolescens, no illum aperiri quo, usu odio brute at. Qui te porro electram, ea dico facete utroque quo. Populo quodsi te eam, wisi everti eos ex, eum elitr altera utamur at. Quodsi convenire mnesarchum eu per, quas minimum postulant per id.</p>-->
<#--            </div>-->
            <div class="ui center aligned grid">
                <div class="row"></div>
                <div class="row"></div>
                <div class="row">
                    <div class=" five wide column">
                        <img class="ui fluid  image" src="/template/image/avatar.png" title="" style="">
<#--                        <img class="ui center aligned  image" src="/template/image/avatar.png" title="" style="">居中显示-->
                        <div class="ui button blue  segment ">学号： ${session.user.stuId}</div>

                    </div>

<#--  注意使用了middle aligned 就上下居中了
水平对齐用align="center"
垂直对齐用Valign="middle"
-->
                    <div class="ten wide  celled column middle aligned ">

                        <table class="ui  red table">
                            <thead>
                            <tr>
                                <th class="six wide"></th>
                                <th class="ten wide"></th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td>姓名</td>
                                <td>${session.user.name}</td>
                            </tr>
                            <tr>
                                <td>性别</td>
                                <td>${session.user.gender}</td>
                            </tr>
                            <tr>
                                <td>身份证号码</td>
                                <td>${session.user.ID}</td>
                            </tr>
                            <tr>
                                <td>电话号码</td>
                                <td>${session.user.tel}</td>
                            </tr>
                            <tr>
                                <td>班级</td>
                                <td>${session.user.className}</td>
                            </tr>
                            <tr>
                                <td>地址</td>
                                <td>${session.user.address}</td>
                            </tr>
                            <tr>
                                <td>密码</td>
                                <td>${session.user.password}</td>
                            </tr>
                            </tbody>

                            <tfoot class="full-width">
                            <tr>
                                <th></th>
                                <th colspan="4">
                                    <div class="ui middle aligned small primary labeled icon button">
                                        <i class="user icon"></i> 修改个人信息
                                    </div>

                                </th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>







</div>

</body>

<script>
    $('.ui.accordion')
            .accordion()
    ;
</script>
</html>