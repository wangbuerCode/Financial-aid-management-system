<#-- @ftlvariable name="apply" type="model.Apply" -->
<@override name="title">申请助学金</@override>
<@override name="content">

	<div class="ui left aligned grid">
	<div class="row"></div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">
			<form class="ui large form" id="apply-form">

				<table class="ui  celled table collapsing fixed   ">
					<thead style="display: none">
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>

					</tr>
					</thead>

					<tbody>
					<tr>
						<td class="two wide center aligned">姓名</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="name" value="${student.name}" disabled="disabled">
							</div>
						</td>
						<td class="two wide center aligned ">学号</td>
						<td class="three wide disabled">
							<div class="field">
								<input type="text" name="stuId" value="${student.stuId}">
							</div>
						</td>
						<td class="two wide center aligned">身份证号码</td>
						<td class=" three wide disabled">
							<div class="field">
								<input type="text" name="ID" value="${student.ID}">
							</div>
						</td>
					</tr>

					<tr>
						<td class="two wide center aligned">电话</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="tel" value="${student.tel}">
							</div>
						</td>
						<td class="two wide center aligned ">是否为低保</td>
                        <#--						style="overflow:visible;" 防止被遮挡，-->
						<td class="three wide " style="overflow:visible;">
							<div class="field ">
								<select class="ui dropdown  " name="IsDibao">
									<option value="${apply.isDibao}">是否为低保</option>
									<option value="1">是</option>
									<option value="0">否</option>
								</select>
							</div>
						</td>
						<td class="two wide center aligned">是否建档立卡</td>
						<td class=" three wide" style="overflow:visible;">
							<div class="field">
								<select class="ui dropdown" name="IsJiandang">
									<option value="${apply.isJiandang}">是否建档立卡</option>
									<option value="1">是</option>
									<option value="0">否</option>
								</select>
							</div>
						</td>
					</tr>

					<tr>
						<td class="two wide center aligned">父亲姓名</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="fatherName" value="${apply.fatherName!'无'}">
							</div>
						</td>
						<td class="two wide center aligned ">联系方式</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="fatherTel" value="${apply.fatherTel!'无'}">
							</div>
						</td>
						<td class="two wide center aligned">工作单位</td>
						<td class=" three wide">
							<div class="field">
								<input type="text" name="fatherJob" value="${apply.fatherJob!'无'} ">
							</div>
						</td>
					</tr>

					<tr>
						<td class="two wide center aligned">母亲姓名</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="motherName" value="${apply.motherName!'无'}">
							</div>
						</td>
						<td class="two wide center aligned ">联系方式</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="motherTel" value="${apply.motherTel!'无'}">
							</div>
						</td>
						<td class="two wide center aligned">工作单位</td>
						<td class=" three wide">
							<div class="field">
								<input type="text" name="motherJob" value="${apply.motherJob!'无'}">
							</div>
						</td>
					</tr>

					<tr>
						<td class="two wide center aligned">其他家属姓名</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="other1Name" value="${apply.other1Name!'无'}">
							</div>
						</td>
						<td class="two wide center aligned ">联系方式</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="other1Tel" value="${apply.other1Tel!'无'}">
							</div>
						</td>
						<td class="two wide center aligned">工作单位</td>
						<td class=" three wide">
							<div class="field">
								<input type="text" name="other1Job" value="${apply.other1Job!'无'}">
							</div>
						</td>
					</tr>

					<tr>
						<td class="two wide center aligned">其他家属姓名</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="other2Name" value="${apply.other2Name!'无'}">
							</div>
						</td>
						<td class="two wide center aligned ">联系方式</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="other2Tel" value="${apply.other2Tel!'无'}">
							</div>
						</td>
						<td class="two wide center aligned">工作单位</td>
						<td class=" three wide">
							<div class="field">
								<input type="text" name="other2Job" value="${apply.other2Job!'无'}">
							</div>
						</td>
					</tr>

					<tr>
						<td class="two wide center aligned">家庭月收入</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="Familyincome" value=" ${apply.familyincome} ">
							</div>
						</td>
						<td class="two wide center aligned ">家庭地址</td>
						<td class="three wide ">
							<div class="field">
								<input type="text" name="familyAddress" value="${apply.familyAddress}">
							</div>
						</td>
						<td class="two wide center aligned">助学金等级</td>
						<td class=" three wide" style="overflow:visible;">
							<div class="field">
								<select class="ui dropdown" name="grantLevel">
									<option value="${apply.grantLevel}">申请等级</option>
									<option value="1">一等</option>
									<option value="2">二等</option>
									<option value="3">三等</option>
								</select>
							</div>
						</td>
					</tr>

					<tr>
						<td class="center aligned">申请理由</td>
						<td colspan="5">
							<div class="field text-area ">
								<input type="textarea" name="applyReason" style="width:100%; height:100px;"
								          id="applyReason"  value="${apply.applyReason}" >
							</div>
						</td>
					</tr>

					<tr>
						<td colspan="4">
							<div class="ui error message"></div>
						</td>
						<td>
							<input type="hidden" name="applyId" value="${apply.applyId}"><!--此语句起到一个传递ID的作用-->
						</td>
						<td>
							<div class="ui fluid large teal submit button ">保存</div>
						</td>
					</tr>
					</tbody>
				</table>
			</form>
		</div>
	</div>

	<script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                tel: {
                    rules: [{
                        type: 'empty',
                        prompt: '电话信息不能为空'
                    }]
                },
                IsDibao: {
                    rules: [
                        {
                            type: 'empty',
                            prompt: '低保未选择'
                        }]
                },
                IsJiandang: {
                    rules: [{
                        type: 'empty',
                        prompt: '建档立卡未选择'
                    }]
                },
                fatherName: {
                    rules: [{
                        type: 'empty',
                        prompt: '父亲项目不能为空'
                    }]
                },
                fatherTel: {
                    rules: [{
                        type: 'empty',
                        prompt: '父亲电话不能为空'
                    }]
                },
                fatherJob: {
                    rules: [{
                        type: 'empty',
                        prompt: '父亲单位不能为空'
                    }]
                },
                motherName: {
                    rules: [{
                        type: 'empty',
                        prompt: '母亲姓名不能为空'
                    }]
                },
                motherTel: {
                    rules: [{
                        type: 'empty',
                        prompt: '母亲电话不能为空'
                    }]
                },
                motherJob: {
                    rules: [{
                        type: 'empty',
                        prompt: '母亲单位地址不能为空'
                    }]
                },
                other1Name: {
                    rules: [{
                        type: 'empty',
                        prompt: '家属1姓名不能为空'
                    }]
                },
                other1Tel: {
                    rules: [{
                        type: 'empty',
                        prompt: '家属1电话不能为空'
                    }]
                },
                other1Job: {
                    rules: [{
                        type: 'empty',
                        prompt: '家属1工作单位地址不能为空'
                    }]
                },
                other2Name: {
                    rules: [{
                        type: 'empty',
                        prompt: '家属2姓名不能为空'
                    }]
                },
                other2Tel: {
                    rules: [{
                        type: 'empty',
                        prompt: '家属2电话不能为空'
                    }]
                },
                other2Job: {
                    rules: [{
                        type: 'empty',
                        prompt: '家属2工作单位地址不能为空'
                    }]
                },
                Familyincome: {
                    rules: [{
                        type: 'empty',
                        prompt: '家庭收入不能为空'
                    }]
                },
                familyAddress: {
                    rules: [{
                        type: 'empty',
                        prompt: '家庭住址不能为空'
                    }]
                },
                grantLevel: {
                    rules: [{
                        type: 'empty',
                        prompt: '申请助学金等级不能为空'
                    }]
                },
                applyReason: {
                    rules: [{
                        type: 'empty',
                        prompt: '申请理由不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/student/modify_applyinfo/',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/student/totalApply'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

	</script>

</@override>
<@extends name="../stu_layout.ftl"></@extends>