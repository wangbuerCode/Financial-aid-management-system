<#-- @ftlvariable name="array" type="java.util.Arrays" -->
<#-- @ftlvariable name="apply" type="model.Apply" -->
<@override name="title">申请详情</@override>
<@override name="content">


	<div class="ui left aligned grid">
	<div class="row"></div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">
			<form class="ui large form" id="apply-form">

				<table class="ui  celled table collapsing fixed   ">
					<thead style="display: none">
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>

					</tr>
					</thead>

					<tbody>
					<tr>
						<td class="two wide center aligned">姓名</td>
						<td class="three wide disabled">${apply.name}</td>
						<td class="two wide center aligned ">学号</td>
						<td class="three wide center aligned">${apply.stuId}</td>
						<td class="two wide disabled">身份证号码</td>
						<td class="three wide center aligned ">${apply.ID}</td>
					</tr>

					<tr>
						<td class="two wide center aligned">电话</td>
						<td class="three wide disabled">${apply.tel}</td>
						<td class="two wide center aligned ">是否低保</td>
                        <#if (apply.isDibao )==0>
							<td class="three wide center aligned">否</td>
                        <#else >
							<td class="three wide center aligned">是</td>
                        </#if>
						<td class="two wide disabled">是否建档立卡</td>
                        <#if (apply.isJiandang )==0>
							<td class="three wide center aligned">否</td>
                        <#else >
							<td class="three wide center aligned">是</td>
                        </#if>
					</tr>

					<tr>
						<td class="two wide center aligned">父亲姓名</td>
						<td class="three wide disabled">${apply.fatherName}</td>
						<td class="two wide center aligned">联系方式</td>
						<td class="three wide disabled">${apply.fatherTel}</td>
						<td class=" two wide ">工作单位</td>
						<td class=" three wide center aligned disabled">${apply.fatherJob}</td>
					</tr>

					<tr>
						<td class="two wide center aligned">母亲姓名</td>
						<td class="three wide disabled">${apply.motherName}</td>
						<td class="two wide center aligned">联系方式</td>
						<td class="three wide disabled">${apply.motherTel}</td>
						<td class=" two wide ">工作单位</td>
						<td class=" three wide center aligned disabled">${apply.motherJob}</td>
					</tr>

					<tr>
						<td class="two wide center aligned">兄弟姓名</td>
						<td class="three wide disabled">${apply.other1Name}</td>
						<td class="two wide center aligned">联系方式</td>
						<td class="three wide disabled">${apply.other1Tel}</td>
						<td class=" two wide ">工作单位</td>
						<td class=" three wide center aligned disabled">${apply.other1Job}</td>
					</tr>

					<tr>
						<td class="two wide center aligned">姊妹姓名</td>
						<td class="three wide disabled">${apply.other2Name}</td>
						<td class="two wide center aligned">联系方式</td>
						<td class="three wide disabled">${apply.other2Tel}</td>
						<td class=" two wide ">工作单位</td>
						<td class=" three wide center aligned disabled">${apply.other2Job}</td>
					</tr>

					<tr>
						<td class="two wide center aligned">家庭月收入</td>
						<td class="three wide disabled">${apply.familyincome}</td>
						<td class="two wide center aligned ">家庭地址</td>
						<td class="three wide center aligned disabled">${apply.familyAddress}</td>
						<td class="two wide disabled">申请助学金等级</td>

						<td class="three wide center aligned disabled">${apply.grantLevel}</td>
					</tr>

					<tr>
						<td class="center aligned">申请理由</td>
						<td colspan="5">
							<div class="field text-input ">
								<p class="ui text">${apply.applyReason}</p>
							</div>
						</td>
					</tr>

					</tbody>
				</table>
			</form>
		</div>
	</div>


</@override>
<@extends name="../stu_layout.ftl"></@extends>