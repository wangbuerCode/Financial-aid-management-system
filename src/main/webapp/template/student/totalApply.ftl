<#-- @ftlvariable name="applies" type="java.util.List<model.Apply>" -->

<@override name="title">申请信息汇总页</@override>
<@override name="content">

	<div class="ui left aligned grid">
	<div class="row"></div>
	<div class="row"></div>
	<div class="row"></div>
	<div class="row"></div>
	<div class="row"></div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" twelve wide  grid column ">

			<table class="ui  celled table collapsing fixed   ">
                <#if ! applies.empty >
				<thead>
				<tr>
					<th class="center aligned">申请序号</th>
					<th class="center aligned">学号</th>
					<th class="center aligned">助学金等级</th>
					<th class="center aligned">申请理由</th>
					<th class="center aligned">审核状态</th>
					<th class="center aligned" colspan="3">操作</th>
				</tr>
				</thead>

				<tbody>
				<tr>
					<td class="two wide center aligned">${applies.get(0).applyId}</td>
					<td class="two wide center aligned ">${applies.get(0).stuId}</td>
					<td class="two wide center aligned">${applies.get(0).grantLevel}</td>
					<td class="four wide center aligned">${applies.get(0).applyReason}</td>
                    <#if applies.get(0).status ==0>
						<td class="two wide center aligned ">未审核</td>
                    <#elseif applies.get(0).status ==1>
						<td class="two wide center aligned">审核通过</td>
                    <#elseif applies.get(0).status ==2>
						<td class="two wide center aligned">审核未通过</td>
                    </#if>
					<td class="four wide center aligned">
						<a class="ui button blue" href="${base}/student/search_info">详情</a>
                        <#if applies.get(0).status == 0>
							<a class="ui green button"
							   href="${base}/student/modify_info/${applies.get( 0).applyId!""}">修改</a>
	                        <a class="ui red  button"
	                           href="${base}/student/delete_info/${applies.get(0).applyId!""}">删除</a>
                        <#else>
							<a class="ui green button disabled"
							   href="${base}/student/modify_info/${applies.get( 0).applyId!""}">修改</a>
	                        <a class="ui red  button disabled"
	                           href="${base}/student/delete_info/${applies.get(0).applyId!""}">删除</a>
                        </#if>

					</td>
                    <#else >
						<div class="ui negative massive message">
							<div class="header">
								未查询到你的助学金申请信息
							</div>
							<p>请重新申请
							</p></div>
                    </#if>
				</tr>

				</tbody>
			</table>

		</div>

	</div>




</@override>
<@extends name="../stu_layout.ftl"></@extends>