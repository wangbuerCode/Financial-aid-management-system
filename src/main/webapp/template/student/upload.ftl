<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<script src="${base}/scripts/jquery.min.js"></script>
	<script src="${base}/scripts/semantic.min.js"></script>
	<link rel="stylesheet" href="${base}/styles/semantic.min.css"/>
	<title>上传测试</title>
</head>
<style>
	.a-upload {
		padding: 4px 10px;
		height: 20px;
		line-height: 20px;
		position: relative;
		cursor: pointer;
		color: #888;
		background: #fafafa;
		border: 1px solid #ddd;
		border-radius: 4px;
		overflow: hidden;
		display: inline-block;
		*display: inline;
		*zoom: 1
	}

	.a-upload  input {
		position: absolute;
		font-size: 100px;
		right: 0;
		top: 0;
		opacity: 0;
		filter: alpha(opacity=0);
		cursor: pointer
	}

	.a-upload:hover {
		color: #444;
		background: #eee;
		border-color: #ccc;
		text-decoration: none
	}
</style>
<body>

<#--<form method="post" action="/student/upload" enctype="multipart/form-data">-->
<#--	<input type="file" name="file">-->
<#--	<input type="submit" value="上传">-->
<#--</form>-->



<#--<form class="ui  form "   enctype="multipart/form-data" >-->
<#--	<div class="ui stacked segment">-->
<#--		<div class="field">-->
<#--			<div class="ui left icon input">-->
<#--				<i class="user icon"></i>-->
<#--				<input type="file" name="file" placeholder="请上传文件">-->
<#--			</div>-->
<#--		</div>-->

<#--		<div class="ui error message"></div>-->
<#--		<div class="ui fluid large teal submit button">上传</div>-->
<#--	</div>-->
<#--</form>-->

<!-- ajax上传 -->

<#--<form class="ui form" id="form">-->

	<input type="file" name="file"><br>
	<input type="button" value="ajax上传" onclick="upload()">
</form>


</body>
<script type="text/javascript">



    function upload() {
        //封装表单数据
        var formData=new FormData($("#form")[0]);
        //发送ajax请求
        $.ajax({
            url:"${base}/student/upload",
            type:"post",
            data:formData,
            //将文件类型设置为二进制类型
            contentType:false,
            processData:false,
            success:function(result){
                alert(result);
            }
        });
    }
</script>
<script>




    $('.ui.form').form({
        fields:{
            file :{
                identifier:'file',
                rules:[
                    {
                        type:'empty',prompt:'文件不能为空'
                    }
                ]
            }
        }
    }).api({
        method:'POST',
        url:'${base}/student/upload',
        serializeForm:true,
	    contentType:false,
        success:function (res) {
            alert(res.message);
            if(res.success){
                if (res.flag==0){
                    <#--window.location.href='${base}/student'-->
                }else {
                    window.location.href='${base}/admin'
                }

            }else{
                $('.ui.form').form('add errors',[res.message]);
            }
        }
    });

</script>

</html>