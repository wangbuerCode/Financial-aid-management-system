<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="${base}/scripts/jquery.min.js"></script>
    <script src="${base}/scripts/semantic.min.js"></script>
    <link rel="stylesheet" href="${base}/styles/semantic.min.css"/>
    <title>用户登录</title>

    <style type="text/css">
        body {
            background-color: #DADADA;
            background-image:url("${base}/template/image/login-back.jpg");
        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
        .mybox{
            position: relative;
        }
        .mybox span{
            position:absolute;
            display: block;
            height: 34px;
            right:2px;
            top:2px;
            line-height: 34px;
        }
        #content_font_size{
            font-size: 30px;
        }
    </style>

</head>
<body>


<div class="ui middle aligned center aligned grid">
    <div class="column">
        <h2 class="ui teal image header">
            <#--<img src="${base}image/avatar.png" class="image">-->
            <div class="content" id="content_font_size">

            </div>
        </h2>


        <form class="ui large form"  id="login-form">
            <div class="ui stacked segment">
                <div class="field">
                    <div class="ui content  " >
                        <div style="font-size: 18px;color: #00b5ad;">贫困生管理系统</div>
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="username" placeholder="请输入账户">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="请输入密码">
                    </div>
                </div>


<#--                <div class="mybox">-->
<#--                    <div class="field grid eleven wide red">-->
<#--                    <input type="text" name="captcha" placeholder="请输入验证码" autocomplete="off">-->
<#--                    </div>-->
<#--                    <span> <img src="${base}/captcha" id="captcha-img" onclick="refreshCaptcha()"></span>-->
<#--                </div>-->

                <div class="ui error message"></div>
                <div class="ui fluid large teal submit button">登陆</div>
            </div>
        </form>

        <div class="ui message">
            New to us? <a href="${base}/register">学生注册</a>
        </div>
    </div>

</div>
<div class="ui text center aligned container footer  " style="font-weight: 800;font-size: 24px"></div>

</body>
<script>
    $('.ui.form').form({
        fields:{
            username :{
                identifier:'username',
                rules:[
                    {
                        type:'empty',prompt:'用户名不能为空'
                    }
                ]
            },
            password: {
                identifier: 'password',
                rules: [{
                    type: 'empty', prompt: '密码不能为空'
                }]
            }
        }
    }).api({
        method:'POST',
        url:'${base}/loginCheck',
        serializeForm:true,
        // contentType:"application/x-www-form-urlencoded:charset=UTF-8",
        success:function (res) {
            alert(res.message);
            if(res.success){
                if (res.flag==0){
                    window.location.href='${base}/student'
                }else {
                    window.location.href='${base}/admin'
                }

            }else{
                $('.ui.form').form('add errors',[res.message]);
            }
        }
    });
    <#--function refreshCaptcha() {-->
    <#--    $('#captcha-img').attr('src','${base}/captcha?a'+Math.random())-->
    <#--}-->
</script>

</html>