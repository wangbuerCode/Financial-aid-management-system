<#-- @ftlvariable name="students" type="java.util.List<model.Student>" -->
<#-- @ftlvariable name="applies" type="java.util.List<model.Apply>" -->

<@override name="title">搜索信息页</@override>
<@override name="content">

	<div class="ui left aligned grid">

	<div class="row">
	</div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">
			<form class="ui fluid form">
				<div class="ui action input">
					<div class="filed">
						<input type="text" placeholder="搜索..." name="search">
					</div>
					<div class="filed">
						<select class="ui dropdown" name="select">
							<option value="">搜索方式</option>
							<option value="1">学号</option>
							<option value="0">姓名</option>
						</select>
					</div>
					<a class="ui primary submit button" >搜索</a><br>
				</div>
				<div class="ui error message"></div>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">

			<table class="ui  celled table collapsing fixed   ">
                <#if ! students.empty >
					<thead>
					<tr>
						<th class="center aligned">学号</th>
						<th class="center aligned">姓名</th>
						<th class="center aligned">性别</th>
						<th class="center aligned">电话</th>
						<th class="center aligned">地址</th>
						<th class="center aligned">班级</th>
						<th class="center aligned">密码</th>
						<th class="center aligned">操作</th>
					</tr>
					</thead>
					<tbody>
					<#list students as students>
						<tr>
						<td class="one wide center aligned">${students.stuId}</td>
						<td class="one wide center aligned">${students.name}</td>
						<td class="one wide center aligned">${students.gender}</td>
						<td class="two wide center aligned">${students.tel}</td>
						<td class="two wide center aligned">${students.address}</td>
						<td class="two wide center aligned">${students.className}</td>
						<td class="two wide center aligned">${students.password}</td>
						<td class="five wide center aligned">
							<a class="ui green button"
							   href="${base}/admin/modify_info/${students.stuId!""}">修改</a>
							<a class="ui red  button"
							   href="${base}/admin/delete_info/${students.stuId!""}">删除</a>
						</td>
						</tr>
                    </#list>
                    <#else >
						<div class="ui negative massive message">
							<div class="header">
								未查询学生信息
							</div>
							<p>请重新输入
							</p></div>
                    </#if>
					</tbody>
			</table>

		</div>
	</div>


	<script>

        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields:{
                search:{
                    rules:[{
                        type:'empty',
                        prompt:'内容不能为空'
                    }]
                },
                select:{
                    rules:[{
                        type:'empty',
                        prompt:'搜索方式不能为空'
                    }]
                }
            }
        }).api({
            method:'POST',
            url:'${base}/admin/searchStu',
            serializeForm:true,
            success:function (res) {
                // alert(res.message);
                window.location.href='${base}/admin/StudentInfoBySearch'
            }
        })


	</script>

</@override>
<@extends name="admin_layout.ftl"></@extends>