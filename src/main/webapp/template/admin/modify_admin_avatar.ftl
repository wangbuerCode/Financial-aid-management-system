<@override name="title">修改个人头像</@override>
<@override name="content">

	<div class="ui center aligned grid">
		<div class="row"></div>
		<div class="row"></div>
		<div class="row">
			<div class=" five wide column">
				<img class="ui   image" src="${admin.avatar}" title="" style=""x="0" y="0" width="570" height="570">
				<form class="ui form" id="form">
					<div class="field">
						<div class="ui left file input">
							<input type="file" name="file" lass="a-upload" id="img-upload">
							<input style="text-align:center;padding:10px 20px;width:300px;" onclick="upload()"
							       type="button" class="ui button primary" value="保存头像">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>



	<script type="text/javascript">

        function upload() {
            //封装表单数据
            var formData = new FormData($("#form")[0]);
            //发送ajax请求
            $.ajax({
                url: "${base}/admin/upload",
                type: "post",
                data: formData,
                //将文件类型设置为二进制类型
                contentType: false,
                processData: false,
                success: function (res) {
                    alert(res.message);
                    window.location.href='${base}/admin/modify_admin_avatar'
                }
            });
        }
	</script>
</@override>
<@extends name="admin_layout.ftl"></@extends>