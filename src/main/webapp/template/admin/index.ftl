<#-- @ftlvariable name="admin" type="model.Admin" -->
<@override name="title">管理员首页</@override>
<@override name="content">

	<div class="ui center aligned grid">
		<div class="row"></div>
		<div class="row"></div>
		<div class="row">
			<div class=" five wide column">
				<img class="ui  image" src="${admin.avatar}" title="" style="" style=""x="0" y="0" width="570" height="570">
                <#--                        <img class="ui center aligned  image" src="/template/image/avatar.png" title="" style="">居中显示-->
				<div class="ui button blue  segment ">工号： ${admin.adminId} </div>

			</div>

            <#--  注意使用了middle aligned 就上下居中了
			水平对齐用align="center"
			垂直对齐用Valign="middle"
			-->
			<div class="ten wide  celled column middle aligned ">

				<table class="ui  red table">
					<thead>
					<tr>
						<th class="six wide"></th>
						<th class="ten wide"></th>
					</tr>
					</thead>

					<tbody>
					<tr>
						<td>姓名</td>
						<td>${admin.name}</td>
					</tr>
					<tr>
						<td>性别</td>
						<td>${admin.gender}</td>
					</tr>
					<tr>
						<td>电话号码</td>
						<td>${admin.tel}</td>
					</tr>
					<tr>
						<td>主管班级</td>
						<td>${admin.className}</td>
					</tr>
					<tr>
						<td>地址</td>
						<td>${admin.address}</td>
					</tr>
					<tr>
						<td>权限</td>
						<#if admin.power=0>
						    <td>超级管理员</td>
							<#else >
							<td>普通管理员</td>
						</#if>

					</tr>
					<tr>
						<td>密码</td>
						<td>${admin.password}</td>
					</tr>
					</tbody>

					<tfoot class="full-width">
					<tr>
						<th>
							<a class="ui middle aligned small primary  button" href="/admin/modify_admin_avatar">
								<i class="user icon"></i> 修改个人头像
							</a>
						</th>
						<th colspan="4">
							<a class="ui middle aligned small primary  button" href="/admin/modify_admin_info">
								<i class="user icon"></i> 修改个人信息
							</a>

						</th>
					</tr>
					</tfoot>
				</table>

			</div>
		</div>
	</div>

</@override>
<@extends name="admin_layout.ftl"></@extends>