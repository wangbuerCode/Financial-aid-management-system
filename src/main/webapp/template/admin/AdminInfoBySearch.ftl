<#-- @ftlvariable name="students" type="java.util.List<model.Student>" -->
<#-- @ftlvariable name="applies" type="java.util.List<model.Apply>" -->

<@override name="title">搜索管理员信息页</@override>
<@override name="content">

	<div class="ui left aligned grid">

	<div class="row">
	</div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">
			<form class="ui fluid form">
				<div class="ui action input">
					<div class="filed">
						<input type="text" placeholder="搜索..." name="search">
					</div>
					<div class="filed">
						<select class="ui dropdown" name="select">
							<option value="">搜索方式</option>
							<option value="1">工号</option>
							<option value="0">姓名</option>
						</select>
					</div>
					<a class="ui primary submit button" >搜索</a><br>
				</div>
				<div class="ui error message"></div>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">

			<table class="ui  celled table collapsing fixed   ">
                <#if ! adminList.empty >
				<thead>
				<tr>
					<th class="center aligned">工号</th>
					<th class="center aligned">姓名</th>
					<th class="center aligned">性别</th>
					<th class="center aligned">电话</th>
					<th class="center aligned">地址</th>
					<th class="center aligned">班级</th>
					<th class="center aligned">权限</th>
					<th class="center aligned">密码</th>
					<th class="center aligned">操作</th>
				</tr>
				</thead>
				<tbody>
                <#list adminList as admin>
					<tr>
						<td class="one wide center aligned">${admin.adminId}</td>
						<td class="one wide center aligned">${admin.name}</td>
						<td class="one wide center aligned">${admin.gender}</td>
						<td class="two wide center aligned">${admin.tel}</td>
						<td class="two wide center aligned">${admin.address}</td>
						<td class="two wide center aligned">${admin.className}</td>
                        <#if admin.power == 0>
							<td class="two wide center aligned">超级管理员</td>
                        <#elseif admin.power == 1>
							<td class="two wide center aligned">普通管理员</td>
                        </#if>
						<td class="two wide center aligned">${admin.password}</td>
						<td class="five wide center aligned">
							<a class="ui green button"
							   href="${base}/admin/modify_admininfo/${admin.adminId!""}">修改</a>
							<a class="ui red  button"
							   href="${base}/admin/delete_admininfo/${admin.adminId!""}">删除</a>
						</td>
					</tr>
                </#list>
                <#else >
					<div class="ui negative massive message">
						<div class="header">
							未查询学生信息
						</div>
						<p>请重新输入
						</p></div>
                </#if>
				</tbody>
			</table>

		</div>
	</div>


	<script>

        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields:{
                search:{
                    rules:[{
                        type:'empty',
                        prompt:'内容不能为空'
                    }]
                },
                select:{
                    rules:[{
                        type:'empty',
                        prompt:'搜索方式不能为空'
                    }]
                }
            }
        }).api({
            method:'POST',
            url:'${base}/admin/searchAdmin',
            serializeForm:true,
            success:function (res) {
                // alert(res.message);
                window.location.href='${base}/admin/AdminInfoBySearch'
            }
        })


	</script>

</@override>
<@extends name="admin_layout.ftl"></@extends>