<#-- @ftlvariable name="students" type="java.util.List<model.Student>" -->
<#-- @ftlvariable name="applies" type="java.util.List<model.Apply>" -->

<@override name="title">助学金申请信息汇总页</@override>
<@override name="content">

	<div class="ui  aligned grid sixteen column">

	<div class="row">
	</div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">
			<td class="four wide center aligned">
				<a class="ui green button"
				   href="${base}/admin/admin_way/0">查看未审核同学</a>
			</td>
			<td class="four wide center aligned">
				<a class="ui green button"
				   href="${base}/admin/admin_way/1">查看审核通过同学</a>
			</td>
			<td class="four wide center aligned">
				<a class="ui green button"
				   href="${base}/admin/admin_way/2">查看审核被拒绝同学</a>
			</td>
		</div>

	</div>

	<div class="row">
		<div class="two wide   column"></div>
		<div class=" eleven wide  column ">

			<table class="ui  celled table collapsing fixed   ">
                <#if ! page.getList().isEmpty() >
					<thead>
					<tr>
						<th class="center aligned">申请序号</th>
						<th class="center aligned">学号</th>
						<th class="center aligned">姓名</th>
						<th class="center aligned">助学金等级</th>
						<th class="center aligned">申请理由</th>
						<th class="center aligned">审核状态</th>
						<th class="center aligned">操作</th>
					</tr>
					</thead>

					<tbody>
                    <#list  page.getList() as apply>
						<tr>
						<td class="one wide center aligned">${apply.applyId}</td>
						<td class="one wide center aligned">${apply.stuId}</td>
						<td class="one wide center aligned">${apply.name}</td>
						<td class="one wide center aligned">${apply.grantLevel}</td>
						<td class="two wide center aligned">${apply.applyReason}</td>
                        <#if apply.status==0>
							<td class="one wide center aligned">未审核</td>
                        <#elseif apply.status==1>
							<td class="one wide center aligned">通过</td>
                        <#elseif apply.status==2>
							<td class="one wide center aligned">被拒绝</td>
                        </#if>
						<td class="three wide center aligned">
							<a class="ui green button"
							   href="${base}/admin/admin_apply_stuinfo/${apply.applyId!""}">详情</a>
                            <#if apply.status==0>
								<a class="ui blue  button"
								   href="${base}/admin/pass/${apply.applyId!""}">通过</a>
								<a class="ui negative  button"
								   href="${base}/admin/refuse/${apply.applyId!""}">拒绝</a>
                            <#elseif apply.status==1>
								<a class="ui blue  button disabled"
								   href="${base}/admin/pass/${apply.applyId!""}">通过</a>
								<a class="ui negative  button"
								   href="${base}/admin/refuse/${apply.applyId!""}">拒绝</a>
                            <#elseif apply.status==2>
								<a class="ui blue  button"
								   href="${base}/admin/pass/${apply.applyId!""}">通过</a>
								<a class="ui negative  button disabled"
								   href="${base}/admin/refuse/${apply.applyId!""}">拒绝</a>
                            </#if>


						</td>
                    <#else >
						<div class="ui negative massive message">
							<div class="header">
								未查询到申请信息
							</div>
							<p>请重新查询
							</p></div>
						</tr>
                    </#list>
					<tr>
						<td colspan="6"></td>
						<td colspan="1" class="center aligned">
                            <#if page.getPageNumber() !=1 >
								<a href="?page=${page.getPageNumber()-1}">上一页</a>
                            </#if>
                            ${page.getPageNumber()}/${page.getTotalPage()}
                            <#if page.getPageNumber() != page.getTotalPage() >
								<a href="?page=${page.getPageNumber()+1}">下一页</a>
                            </#if>

						</td>
					</tr>
					</tbody>

                </#if>
			</table>

		</div>

	</div>


	<script>
        $('.ui.dropdown').dropdown();
	</script>

</@override>
<@extends name="admin_layout.ftl"></@extends>