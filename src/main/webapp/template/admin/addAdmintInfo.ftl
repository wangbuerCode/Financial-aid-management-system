<#-- @ftlvariable name="student" type="model.Student" -->
<@override name="title">添加管理员信息</@override>

<@override name="content">

	<div class="ui left aligned grid">
	<div class="row"></div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">
			<form class="ui large form"  id="login-form">
				<table class="ui  celled table three column">
					<thead>
					<tr>
						<th colspan="1">Key</th>
						<th colspan="2">Value</th>
					</tr></thead>
					<tbody>
					<tr>
						<td>姓名</td>
						<td>
							<div class="field">
								<div class="ui left  input">
									<input type="text" name="username">
								</div>
							</div>

						</td>
					</tr>
					<tr>
						<td>性别</td>
						<td>
							<div class="field">
								<select class="ui dropdown " name="gender">
									<option value="">选择性别</option>
									<option value="男">男</option>
									<option value="女">女</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>电话</td>
						<td>
							<div class="field">
								<div class="ui left  input">
									<input type="text" name="tel" >
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>班级</td>
						<td>
							<div class="field">
								<select class="ui dropdown  " name="class">
									<option value="">选择班级</option>
                                    <#list classes as class>
										<option value="${class.classId!"0"}">${class.className!""} </option>
                                    </#list>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>地址</td>
						<td>
							<div class="field">
								<div class="ui left  input">
									<input type="text" name="address" >
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>权限</td>
						<td>
							<div class="field">
								<div class="ui left  input">
									<select class="ui dropdown " name="power">
										<option value="">选择权限</option>
										<option value="0">超级</option>
										<option value="1">普通</option>
									</select>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>密码</td>
						<td>
							<div class="field">
								<div class="ui left  input">
									<input type="text" name="password" >
								</div>
							</div>
						</td>
					</tr>
                    <#--						<tr>-->
                    <#--							<td>头像</td>-->
                    <#--							<td>-->
                    <#--								<div class="field">-->
                    <#--									<div class="ui left file  input">-->
                    <#--										<input class="ui  button icon-file " type="file" name="avatar"><br>-->
                    <#--&lt;#&ndash;										<input type="button" value="ajax上传" onclick="upload()">&ndash;&gt;-->
                    <#--									</div>-->
                    <#--								</div>-->
                    <#--							</td>-->
                    <#--						</tr>-->
					<tr>
						<td><div class="ui error message"></div></td>

						<td><div class="ui fluid large teal submit button">添加</div></td>
					</tr>
					</tbody>
				</table>
			</form>
		</div>

        <#--  注意使用了middle aligned 就上下居中了
		水平对齐用align="center"
		垂直对齐用Valign="middle"
		-->

	</div>

	<script>
        $('.ui.form').form({
            fields:{
                username :{
                    identifier:'username',
                    rules:[
                        {
                            type:'empty',prompt:'用户名不能为空'
                        }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [{
                        type: 'empty', prompt: '密码不能为空'
                    }]
                },
                gender: {
                    identifier: 'gender',
                    rules: [{
                        type: 'empty', prompt: '性别不能为空'
                    }]
                },
                address: {
                    identifier: 'address',
                    rules: [{
                        type: 'empty', prompt: '地址不能为空'
                    }]
                },
                tel: {
                    identifier: 'tel',
                    rules: [{
                        type: 'empty', prompt: '电话不能为空'
                    }]
                },
                power: {
                    identifier: 'power',
                    rules: [{
                        type: 'empty', prompt: '权限不能为空'
                    }]
                },
                class: {
                    identifier: 'class',
                    rules: [{
                        type: 'empty', prompt: '班级不能为空'
                    }]
                }
            }
        }).api({
            method:'POST',
            url:'${base}/admin/DoAddAdmin',
            serializeForm:true,
            success:function (res) {
                alert(res.message);
                if(res.success){
                    window.location.href='${base}/admin/AdminInfo'
                }else{
                    $('.ui.form').form('add errors',[res.message]);
                }
            }
        });
	</script>


</@override>

<@extends name="admin_layout.ftl"></@extends>