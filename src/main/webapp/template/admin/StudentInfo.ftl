<#-- @ftlvariable name="students" type="java.util.List<model.Student>" -->
<#-- @ftlvariable name="applies" type="java.util.List<model.Apply>" -->

<@override name="title">学生信息</@override>
<@override name="content">

	<div class="ui left aligned grid">

	<div class="row">
	</div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">
			<form class="ui fluid form">
				<div class="ui action input">
					<div class="filed">
					<input type="text" placeholder="搜索..." name="search">
					</div>
					<div class="filed">
						<select class="ui dropdown" name="select">
							<option value="">搜索方式</option>
							<option value="1">学号</option>
							<option value="0">姓名</option>
						</select>
					</div>
					<a class="ui primary submit button" >搜索</a><br>
				</div>
				<div class="ui error message"></div>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">

			<table class="ui  celled table collapsing fixed   ">
                <#if ! page.getList().isEmpty() >
					<thead>
					<tr>
						<th class="center aligned">学号</th>
						<th class="center aligned">姓名</th>
						<th class="center aligned">性别</th>
						<th class="center aligned">电话</th>
						<th class="center aligned">地址</th>
						<th class="center aligned">班级</th>
						<th class="center aligned">密码</th>
						<th class="center aligned">操作</th>
					</tr>
					</thead>

					<tbody>
                    <#list  page.getList() as student>
						<tr>
						<td class="one wide center aligned">${student.stuId}</td>
						<td class="one wide center aligned">${student.name}</td>
						<td class="one wide center aligned">${student.gender}</td>
						<td class="two wide center aligned">${student.tel}</td>
						<td class="two wide center aligned">${student.address}</td>
						<td class="two wide center aligned">${student.className}</td>
						<td class="two wide center aligned">${student.password}</td>
						<td class="five wide center aligned">
							<a class="ui green button"
							   href="${base}/admin/modify_info/${student.stuId!""}">修改</a>
							<a class="ui red  button"
							   href="${base}/admin/delete_info/${student.stuId!""}">删除</a>
						</td>
                    <#else >
						<div class="ui negative massive message">
							<div class="header">
								未查询到学生信息
							</div>
							<p>请重新添加
							</p></div>
						</tr>
                    </#list>
					<tr>
						<td colspan="7"></td>
						<td colspan="1" class="center aligned">
                            <#if page.getPageNumber() !=1 >
								<a href="?page=${page.getPageNumber()-1}">上一页</a>
                            </#if>
                            ${page.getPageNumber()}/${page.getTotalPage()}
                            <#if page.getPageNumber() != page.getTotalPage() >
								<a href="?page=${page.getPageNumber()+1}">下一页</a>
                            </#if>

						</td>
					</tr>
					</tbody>

                </#if>
			</table>

		</div>
	</div>


	<script>

        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields:{
                search:{
                    rules:[{
                        type:'empty',
                        prompt:'内容不能为空'
                    }]
                },
                select:{
                    rules:[{
                        type:'empty',
                        prompt:'搜索方式不能为空'
                    }]
                }
            }
        }).api({
            method:'POST',
            url:'${base}/admin/searchStu',
            serializeForm:true,
            success:function (res) {
                    // alert(res.message);
                window.location.href='${base}/admin/StudentInfoBySearch'
            }
        })

	</script>

</@override>
<@extends name="admin_layout.ftl"></@extends>