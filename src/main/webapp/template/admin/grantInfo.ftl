<#-- @ftlvariable name="students" type="java.util.List<model.Student>" -->
<#-- @ftlvariable name="applies" type="java.util.List<model.Apply>" -->

<@override name="title">助学金发放信息汇总页</@override>
<@override name="content">

	<div class="ui left aligned grid">

	<div class="row">
	</div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">
			<td class="four wide center aligned">
				<a class="ui green button"
				   href="${base}/admin/admin_grant_way/0">查看未发放同学</a>
			</td>
			<td class="four wide center aligned">
				<a class="ui green button"
				   href="${base}/admin/admin_grant_way/1">查看已发放同学</a>
			</td>
		</div>
	</div>

	<div class="row">
		<div class="two wide  grid column"></div>
		<div class=" ten wide  grid column ">

			<table class="ui  celled table collapsing fixed   ">
                <#if ! page.getList().isEmpty() >
					<thead>
					<tr>
						<th class="center aligned">序号</th>
						<th class="center aligned">学生学号</th>
						<th class="center aligned">学生姓名</th>
						<th class="center aligned">助学金等级</th>
						<th class="center aligned">审核人</th>
						<th class="center aligned">审核时间</th>
						<th class="center aligned">发放状态</th>
						<th class="center aligned" colspan="3">操作</th>
					</tr>
					</thead>

					<tbody>
                    <#list  page.getList() as grant>
						<tr>
						<td class="two wide center aligned">${grant.grantId}</td>
						<td class="one wide center aligned">${grant.stuId}</td>
						<td class="one wide center aligned">${grant.stuName}</td>
						<td class="two wide center aligned">${grant.grantLevel}</td>
						<td class="two wide center aligned">${grant.adminName}</td>
	                    <td class="two wide center aligned">${grant.time}</td>
                        <#if grant.grantStatus==0>
							<td class="two wide center aligned">未发放</td>
                        <#elseif grant.grantStatus==1>
							<td class="two wide center aligned">已发放</td>

                        </#if>
						<td class="four wide center aligned">
                            <#if grant.grantStatus==0>
								<a class="ui green  button"
								   href="${base}/admin/grantPass/${grant.grantId!""}">发放</a>
                            <#elseif grant.grantStatus==1>
								<a class="ui red button disabled"
								   href="#">发放</a>
                            </#if>



						</td>
                    <#else >
						<div class="ui negative massive message">
							<div class="header">
								未查询到申请信息
							</div>
							<p>请重新查询
							</p></div>
						</tr>
                    </#list>
					<tr>
						<td colspan="7"></td>
						<td colspan="1" class="center aligned">
                            <#if page.getPageNumber() !=1 >
								<a href="?page=${page.getPageNumber()-1}">上一页</a>
                            </#if>
                            ${page.getPageNumber()}/${page.getTotalPage()}
                            <#if page.getPageNumber() != page.getTotalPage() >
								<a href="?page=${page.getPageNumber()+1}">下一页</a>
                            </#if>

						</td>
					</tr>
					</tbody>

                </#if>
			</table>

		</div>
	</div>


	<script>
        $('.ui.dropdown').dropdown();
	</script>

</@override>
<@extends name="admin_layout.ftl"></@extends>