<#-- @ftlvariable name="classes" type="model.Class[]" -->
<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="${base}/scripts/jquery.min.js"></script>
    <script src="${base}/scripts/semantic.min.js"></script>
    <link rel="stylesheet" href="${base}/styles/semantic.min.css"/>
    <title>注册</title>
    <style>
        .body{
            background: #dfd7ff;
        }

        .margintop{
            margin-top: 100px;
        }

    </style>

    </head>
    <body class="body">
<div class="margintop">
    <div class="ui grid container  "><!--container 是一个容器，里面的内容会自动的居中-->
      <div class="eight wides  centered column" >
            <div class="ui inverted teal segment">

<#--                <form class="ui form">-->
<#--                    <div class="field">-->
<#--                        <label>Empty</label>-->
<#--                        <input name="empty" type="text">-->
<#--                    </div>-->
<#--                    <div class="field">-->
<#--                        <label>Dropdown</label>-->
<#--                        <select class="ui dropdown button" name="dropdown">-->
<#--                            <option value="">Select</option>-->
<#--                            <option value="male">Choice 1</option>-->
<#--                            <option value="female">Choice 2</option>-->
<#--                        </select>-->
<#--                    </div>-->
<#--                    <div class="inline field">-->
<#--                        <div class="ui checkbox">-->
<#--                            <input type="checkbox" name="checkbox">-->
<#--                            <label>Checkbox</label>-->
<#--                        </div>-->
<#--                    </div>-->
<#--                    <div class="ui submit button">Submit</div>-->
<#--                    <div class="ui error message"></div>-->
<#--                </form>-->

    <form  class="ui fluid form">
                    <div class="field">
                        用户名：<input type="text" name="name" id="name" placeholder="请输入姓名" autocomplete="off" >
                    </div>
                    <div class="field">
                        密码：<input type="password" name="password"id="password" placeholder="请输入密码" autocomplete="off">
                    </div>
                    <div class="field">
                        重复密码：<input type="password" name="password2"id="password2" placeholder="请再次输入密码" autocomplete="off">
                    </div>
                    <div class="field">
                        地址：<input type="text" name="address" id="address" placeholder="请输入家庭住址" autocomplete="off">
                    </div>
                    <div class="field">
                        电话：<input type="text" name="tel" id="tel" placeholder="请输入电话号码" autocomplete="off">
                    </div>
                    <div class="field">
                        身份证号码：<input type="text" name="ID" id="ID" placeholder="请输入身份证号码" autocomplete="off">
                    </div>

<#--                    <div class="field">-->
<#--                        <div class="ui dropdown select icon button fluid  " name="class">-->
<#--                            <i class="angle down icon"></i>-->
<#--                            <span class="text">选择班级</span>-->
<#--                            <div class="menu">-->
<#--                                <#list classes as class>-->
<#--                                    <div class="item" data-value=" ">${class.className!""} </div>-->
<#--                                </#list>-->
<#--                            </div>-->
<#--                        </div>-->
<#--                    </div>-->

        <div class="field">
                请选择班级
            <select class="ui dropdown  " name="class">
                <option value="">选择班级</option>
                <#list classes as class>
                    <option value="${class.classId!"0"}">${class.className!""} </option>
                </#list>
            </select>
        </div>

<#--                    <div class="field">-->
<#--                        <div class="ui select dropdown icon button fluid "  name="gender" id="gender">-->
<#--                            <i class="angle down icon"></i>-->
<#--                            <span class="text">选择性别</span>-->
<#--                            <div class="menu">-->
<#--                                <div class="item" data-value="男">男</div>-->
<#--                                <div class="item" data-value="女">女</div>-->
<#--                            </div>-->
<#--                        </div>-->
<#--                    </div>-->

                    <#--————————————下拉框的另一种写法——————————————————-->
        <div class="field">
            请选择性别
            <select class="ui dropdown " name="gender">
                <option value="">选择性别</option>
                <option value="男">男</option>
                <option value="女">女</option>
            </select>
        </div>



                    <div class="ui error message"></div>

                    <a class="ui primary fluid submit button" >注册</a><br>
                    <a class="ui primary fluid reset button" >重置</a>
                </form>

            </div>
      </div>
    </div>
</div>
</body>

<script>

    $('.ui.dropdown').dropdown();

    $('.ui.form').form({
        fields:{
            name:{
            rules:[{
                type:'minLength[3]',
                prompt:'用户名至少包含3个字符'
            }]
            },
            password:{
                rules:[
                    {
                        type   : 'empty',
                        prompt : 'Please enter a password'
                    },
                    {
                        type:'minLength[6]',
                        prompt:'密码至少包含6个字符'
                     }]
            },
            password2:{
                rules:[{
                    type:'match[password]',
                    prompt:'密码不一致'
                }]
            },
            address:{
                rules:[{
                    type:'empty',
                    prompt:'地址不能为空'
                }]
            },
            ID:{
                rules:[{
                    type:'empty',
                    prompt:'身份证号码不能为空'
                }]
            },
            class:{
                rules:[{
                    type:'empty',
                    prompt:'班级不能为空'
                }]
            },
            gender:{
                rules:[{
                    type:'empty',
                    prompt:'性别不能为空'
                }]
            },
            tel:{
                rules:[{
                    type:'empty',
                    prompt:'电话不能为空'
                }]
            }
        }
    }).api({
        method:'POST',
        url:'${base}/doRegister',
        serializeForm:true,
        success:function (res) {
            if(res.success){
                alert(res.message);
                window.location.href='${base}/login'
            }else{
                $('.ui.form').form('add errors',[res.message]);
            }
        }
    })

</script>

</html>